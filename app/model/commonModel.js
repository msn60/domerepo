'user strict';
var sql = require('../../config');
var async = require('async');
var common_function = require('../common/common_function');
var commonModel = function (task) {

};
commonModel.getCityData = function (req, result) {
    req.getConnection(function (error, conn) {
        conn.query("select * from tbl_city ", function (err, res) {
            if (err) {
                console.log("error: ", err);
                result(err, null);
            } else {
                result(null, res);
            }
        })
    })
}
commonModel.getStateData = function (req, result) {
    req.getConnection(function (error, conn) {
        conn.query("select * from tbl_state where country_id = 231", function (err, res) {
            if (err) {
                console.log("error: ", err);
                result(err, null);
            } else {
                result(null, res);
            }

        })
    })
}
commonModel.getLocationList = function (req, result) {
    req.getConnection(function (error, conn) {
        conn.query("select * from tbl_location where company_id = ? AND status = 1", req.session.user.company_id, function (err, res) {
            if (err) {
                console.log("error: ", err);
                result(err, null);
            } else {
                result(null, res);
            }

        })
    })
}
commonModel.getCityDataById = function (req, result) {
    req.getConnection(function (error, conn) {
        conn.query("select * from tbl_city WHERE state_id=?", [req.params.id], function (err, res) {
            if (err) {
                console.log("error: ", err);
                result(err, null);
            } else {
                result(null, res);
            }
        })
    })
}
commonModel.getDriverList = function (req, result) {
    var currentDate = common_function.get_dateTime();
    var shortDay = common_function.getSortDayNameByDate(currentDate);

    req.getConnection(function (error, conn) {
        conn.query(`SELECT e.*, ad.morning_start, ad.morning_end, ad.afternoon_start, ad.afternoon_end, ad.evening_start, ad.evening_end FROM tbl_employee e
                    INNER JOIN tbl_role r ON r.id = e.role AND r.master_role_id = 2
                    INNER JOIN tbl_availability_default ad ON ad.driver_id = e.id AND ad.day = "`+shortDay+`"
                    AND ad.working = 1 WHERE e.company_id = ? AND e.status = 1`, req.session.user.company_id, function (err, res) {
            if (err) {
                console.log("error: ", err);
                result(err, null);
            } else {
                result(null, res);
            }

        })
    })
}
module.exports = commonModel;