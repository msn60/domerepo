'user strict';
var employeeModel = function (task) {

};
employeeModel.getEmployeeData = function (req, result) {
    req.getConnection(function (error, conn) {
        conn.query("select e.*, COUNT(l.id) as total_leave FROM tbl_employee e LEFT JOIN tbl_leave l ON l.employee_id = e.id GROUP BY e.id", function (err, res) {
            if (err) {
                console.log("error: ", err);
                result(err, null);
            } else {
                result(null, res);
            }

        })
    })
}
employeeModel.empCheck = function (req, addArray, result) {
    req.getConnection(function (error, conn) {
        if (req.body.id != '') {
            conn.query("select * from tbl_employee where email = ? and id != ?", [addArray.email, req.body.id], function (err, res) {
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                } else {
                    result(null, res);

                }
            })
        } else {
            conn.query("select * from tbl_employee where email = ?", [addArray.email], function (err, res) {
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                } else {
                    result(null, res);
                }
            })
        }

    })
}
employeeModel.getEmployeeDataById = function (req, result) {
    req.getConnection(function (error, conn) {
        conn.query("select * FROM tbl_employee where id = ? ", req.params.id, function (err, res) {
            if (err) {
                console.log("error: ", err);
                result(err, null);
            } else {
                result(null, res);
            }

        })
    })
}
employeeModel.getLeaveById = function (req, result) {
    req.getConnection(function (error, conn) {
        conn.query("select * FROM tbl_leave where employee_id = ? ", req.params.id, function (err, res) {
            if (err) {
                console.log("error: ", err);
                result(err, null);
            } else {
                result(null, res);
            }

        })
    })
}

employeeModel.createEmployee = function (req, addArray, result) {
    req.getConnection(function (error, conn) {
        if (req.body.id != '') {
            conn.query("UPDATE tbl_employee SET ? WHERE id=?", [addArray, req.body.id], function (err, res) {
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                } else {
                    result(null, res);
                }
            })
        }
        else {
            conn.query("INSERT INTO tbl_employee SET ? ", addArray, function (err, res) {
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                } else {
                    result(null, res);
                }
            })
        }

    })
}

employeeModel.createLeave = function (req, addArray, result) {
    req.getConnection(function (error, conn) {
        if (req.body.id != '') {
            conn.query("UPDATE tbl_leave SET ? WHERE id=?", [addArray, req.body.id], function (err, res) {
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                } else {
                    result(null, res);
                }
            })
        }
        else {
            conn.query("INSERT INTO tbl_leave SET ? ", addArray, function (err, res) {
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                } else {
                    result(null, res);
                }
            })
        }

    })
}


employeeModel.deleteEmployee = function (req, result) {
    req.getConnection(function (error, conn) {
        conn.query("delete from tbl_employee where id = ? ", req.params.id, function (err, res) {
            if (err) {
                console.log("error: ", err);
                result(err, null);
            } else {
                result(null, res);
            }

        })
    })
}

module.exports = employeeModel;