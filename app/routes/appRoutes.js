'use strict';

module.exports = function (app) {
  var login = require('../controller/loginController');
  var Employee = require('../controller/employeeController');
  

  app.route('/').get(login.login);
  app.route('/login/').get(login.login);
  app.route('/login/process').post(login.process);
  app.route('/login/logout').get(login.logout);

  app.route('/employee').get(Employee.employee);
  app.route('/employee/add').get(Employee.addEmployee);
  app.route('/employee/edit/:id').get(Employee.editEmployee);
  app.route('/employee/create').post(Employee.createEmployee);
  app.route('/employee/delete/:id').get(Employee.deleteEmployee);
  
  app.route('/leave/add').get(Employee.addLeave);
  app.route('/leave/create').post(Employee.createLeave);
  app.route('/leaves/:id').get(Employee.getLeavesById);

};
