'use strict';
var employee = require('../model/employeeModel');
var common = require('../common/common.js');
var commonFiles = common.commonCssFiles();
var common_function = require('../common/common_function');
var encrypt = require('../common/encrypt');
const { Validator } = require('node-input-validator');


/*Show the form for creating a new resource.*/

exports.employee = function (req, res, next) {

    if (req.session.user !== undefined) {
        employee.getEmployeeData(req, function (err, response) {
            res.render('employee', {
                title: 'Employee',
                page_name: 'employee',
                sessionData: req.session.user,
                access_permission: req.permission,
                data: response,
                commonCss: commonFiles[0],
                commonJs: commonFiles[1],
                common_function: common_function,
                encrypt: encrypt,
                cdnCSS: [],
                cdnJs: [],
                csslinks: [],
                jslinks: ['employee']
            })
        })
    } else {
        res.redirect(base_url + '/login/');
    }
}


exports.addEmployee = function (req, res, next) {
    if (req.session.user !== undefined) {
                    res.render('employee_add', {
                        title: 'Add_employee',
                        page_name: 'employee_add',
                        sessionData: req.session.user,
                        access_permission: req.permission,
                        employee: [],
                        commonCss: commonFiles[0],
                        commonJs: commonFiles[1],
                        common_function: common_function,
                        encrypt: encrypt,
                        cdnCSS: [],
                        cdnJs: [],
                        csslinks: [],
                        jslinks: ['employee']
                    })
    } else {
        res.redirect(base_url + '/login/');
    }
}

exports.editEmployee = function (req, res, next) {
    if (req.session.user !== undefined) {
            employee.getEmployeeDataById(req, function (err, employeeData) {
                        res.render('employee_add', {
                            title: 'Edit_employee',
                            page_name: 'employee_add',
                            sessionData: req.session.user,
                            access_permission: req.permission,
                            employee: employeeData[0],
                            commonCss: commonFiles[0],
                            commonJs: commonFiles[1],
                            common_function: common_function,
                            encrypt: encrypt,
                            cdnCSS: [],
                            cdnJs: [],
                            csslinks: [],
                            jslinks: ['employee']
                        })
                      })
    } else {
        res.redirect(base_url + '/login/');
    }
}



exports.createEmployee = async function (req, res, next) {

    var validationArray = {
        first_name: 'required|alpha|minLength:4',
        last_name: 'required|alpha|minLength:4',
        email: 'required|email',
        salary: 'required|integer'
    }
    if (req.body.password != undefined) {
        validationArray.password = 'required|maxLength:6';
        validationArray.confirm_password = 'required|maxLength:6|same:password';
    }
    const v = await new Validator(req.body, validationArray);

    const matched = await v.check();
    if (!matched) {
        res.status(200).send({ error: true, datamsg: true, check: v, msg: 'All mandatory fiels are required!', data: v.errors })
    } else {
        var addArray = {
            first_name: req.sanitize('first_name').trim(),
            last_name: req.sanitize('last_name').trim(),
            email: encrypt.encrypt(req.sanitize('email').trim()),
            salary: req.sanitize('salary').trim(),
        }
        if (req.body.password != undefined) {
            addArray.password = encrypt.encrypt(req.sanitize('password').trim());
        }
        employee.empCheck(req, addArray, async function (err, response) {
            if (err) {
                res.status(200).send({ error: true, datamsg: false, msg: 'Something went wrong!' });
            } else {
                if (response.length > 0) {
                    res.status(200).send({ error: true, datamsg: false, msg: 'Employee already exists.' });
                } else {
                    employee.createEmployee(req, addArray, async function (err, response) {
                        if (err) {
                            res.status(200).send({ error: true, datamsg: false, msg: 'Something went wrong!' });
                        } else {
                            if (req.body.id != '') {
                                res.status(200).send({ error: false, datamsg: false, msg: 'Employee updated successfully!' });
                            } else {
                                res.status(200).send({ error: false, datamsg: false, msg: 'Employee created successfully!' });
                            }
                        }
                    });
                }
            }
        });
    }
}
exports.deleteEmployee = async function (req, res, next) {
    employee.deleteEmployee(req, async function (err, response) {
        if (err) {
            res.status(200).send({ error: true, datamsg: false, msg: 'Something went wrong!' });
        } else {
            res.status(200).send({ error: false, datamsg: false, msg: 'Employee deleted successfully!' });
        }

    });
}


exports.addLeave = async function (req, res, next) {
    if (req.session.user !== undefined) {
        await employee.getEmployeeData(req, function (err, employeeData) {
                    res.render('leave_add', {
                        title: 'Add Leave',
                        page_name: 'leave_add',
                        sessionData: req.session.user,
                        access_permission: req.permission,
                        commonCss: commonFiles[0],
                        commonJs: commonFiles[1],
                        common_function: common_function,
                        employee: employeeData,
                        cdnCSS: [],
                        cdnJs: [],
                        csslinks: [],
                        jslinks: ['employee']
                    })
                    })
    } else {
        res.redirect(base_url + '/login/');
    }
}

exports.createLeave = async function (req, res, next) {
    const v = await new Validator(req.body, {
          leave_type: 'required',
          leave_date: 'required',
          employee_name: 'required',
          reason: 'required|alphaDash|minLength:10',
          });
          
      const matched = await v.check();
      if (!matched) {
          res.status(200).send({ error: true, datamsg: true, check: v, msg: 'All mandatory fiels are required!', data: v.errors })
      } else {
          
          var addArray = {
            leave_type: req.sanitize('leave_type').trim(),
            leave_date: req.sanitize('leave_date').trim(),
            employee_id: req.sanitize('employee_name').trim(),
            reason: req.sanitize('reason').trim(),
          }
          console.log(addArray);
                      employee.createLeave(req, addArray, async function (err, response) {
                          if (err) {
                              res.status(200).send({ error: true, datamsg: false, msg: 'Something went wrong!' });
                          } else {
                              if (req.body.id != '') {
                                  res.status(200).send({ error: false, datamsg: false, msg: 'Leave updated successfully!' });
                              } else {
                                  res.status(200).send({ error: false, datamsg: false, msg: 'Leave created successfully!' });
                              }
                          }
                      });
  
  
      }
  
  }

  exports.getLeavesById = async function (req, res, next) {

    if (req.session.user !== undefined) {
        await employee.getLeaveById(req, function (err, response) {
            console.log(response);
            res.render('leaves', {
                title: 'Leaves',
                page_name: 'leaves',
                sessionData: req.session.user,
                access_permission: req.permission,
                data: response,
                commonCss: commonFiles[0],
                commonJs: commonFiles[1],
                common_function: common_function,
                cdnCSS: [],
                cdnJs: [],
                csslinks: [],
                jslinks: ['employee']
            })
        })
    } else {
        res.redirect(base_url + '/login/');
    }
}