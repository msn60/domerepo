'use strict';
// var express = require('express')
// var app = express()
var Login = require('../model/loginModel.js');
var common = require('../common/common.js');
var encryption = require('../common/encrypt.js');
var common_function = require('../common/common_function.js');
var commonFiles = common.commonCssFiles();
const { Validator } = require('node-input-validator');
var async = require('async');

exports.login = async function (req, res, next) {

    if (req.session.user) {
        res.redirect(base_url + '/employee');
    } else {
        res.render('login', {
            title: 'Login',
            page_name: 'login',
            commonCss: commonFiles[0],
            commonJs: commonFiles[1],
            cdnCSS: [],
            cdnJs: [],
            csslinks: ['login'],
            jslinks: ['login']
        })
    }

}

/* Logout process */

exports.logout = async function (req, res, next) {
    req.session.destroy(function () {
    });
    res.redirect(base_url + '/login/');

}

/* Login process */

exports.process = async function (req, res, next) {

    const v = await new Validator(req.body, {
        email: 'required|email',
        password: 'required|minLength:6',
    });
    const matched = await v.check();
    if (!matched) {
        res.status(200).send({ error: true, datamsg: true, check: v, msg: 'All mandatory fiels are required!', data: v.errors })
    } else {
        var addArray = {
            email: encryption.encrypt(req.sanitize('email').trim()),
            password: encryption.encrypt(req.body.password)
        }
        Login.check(req, addArray, async function (err, response) {
            if (err) {
                res.status(200).send({ error: true, datamsg: false, errormessage: err, msg: 'Something went wrong!' });
            } else {
                if (response.length > 0) {
                            req.session.user = response[0];
                            req.session.save();
                        res.status(200).send({ error: false, datamsg: false, msg: 'Login success...' });
                } else {
                    res.status(200).send({ error: true, datamsg: false, msg: 'Email or password is wrong!' });
                }
            }
        });


    }

}
/* Check password */