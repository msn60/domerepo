'use strict';
// Nodejs encryption with CTR
var dateFormat = require('dateformat');
const crypto = require('crypto');
var encrypt = require('./encrypt');


var common_function = function (task) {

};
common_function.generate_otp = function (min, max) {
        return Math.floor(
                Math.random() * (max - min + 1) + min
        )
}

common_function.convertToSlug = function (Text) {

        return Text
                .toLowerCase()
                .replace(/[^\w ]+/g, '')
                .replace(/ +/g, '-')
                ;
}
common_function.arr_diff = function (a1, a2) {

        var a = [], diff = [];

        for (var i = 0; i < a1.length; i++) {
                a[a1[i]] = true;
        }

        for (var i = 0; i < a2.length; i++) {
                if (a[a2[i]]) {
                        delete a[a2[i]];
                } else {
                        a[a2[i]] = true;
                }
        }

        for (var k in a) {
                diff.push(k);
        }

        return diff;
}
common_function.getSetting = function (req) {

        // req.getConnection(function (error, conn) {
        //         conn.query("select * from tbl_admin_setting where id = 1", function (err, seting) {
        //                 if (err) {
        //                         console.log("error: ", err);
        //                 } else {
        //                         return seting[0];
        //                 }
        //         })
        // })
}

common_function.get_dateTime = function () {
        let date_ob = new Date();
        // current date
        // adjust 0 before single digit date
        let date = ("0" + date_ob.getDate()).slice(- 2);
        // current month
        let month = ("0" + (date_ob.getMonth() + 1)).slice(- 2);
        // current year
        let year = date_ob.getFullYear();
        // current hours
        let hours = date_ob.getHours();
        // current minutes
        let minutes = date_ob.getMinutes();
        // current seconds
        let seconds = date_ob.getSeconds();
        // prints date in YYYY-MM-DD format
        //console.log(year + "-" + month + "-" + date);
        // prints date & time in YYYY-MM-DD HH:MM:SS format
        return year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds;
}
common_function.formateDate = function (originalDate) {
        if (originalDate == null) {
                return 'NA';
        }
        let date_ob = originalDate;
        // current date
        // adjust 0 before single digit date
        let date = ("0" + date_ob.getDate()).slice(- 2);
        // current month
        let month = ("0" + (date_ob.getMonth() + 1)).slice(- 2);
        // current year
        let year = date_ob.getFullYear();
        // current hours
        let hours = date_ob.getHours();
        // current minutes
        let minutes = date_ob.getMinutes();
        // current seconds
        let seconds = date_ob.getSeconds();
        // prints date in YYYY-MM-DD format
        //console.log(year + "-" + month + "-" + date);
        // prints date & time in YYYY-MM-DD HH:MM:SS format
        return month + "/" + date + "/" + year;
}
common_function.get_date = function (date) {
        return dateFormat(date, "isoDate");
}
common_function.sorting_array = function (array) {
        return array.sort((a, b) => (parseFloat(clear_kw(b.Installed_size)) + parseFloat(clear_kw(b.approved_size)) + parseFloat(clear_kw(b.closed_size))) - (parseFloat(clear_kw(a.Installed_size)) + parseFloat(clear_kw(a.approved_size)) + parseFloat(clear_kw(a.closed_size))))
}

common_function.getDateFilter = function (type) {
        if (type == 'Month') {
                var date = {
                        start_date: getStartDate(0),
                        end_date: getEndDate(0)
                }
                return date;
        } else if (type == 'Today') {
                var date = {
                        start_date: getEndDate(),
                        end_date: getEndDate()
                }
                return date;
        } else if (type == 'Yesterday') {
                var date = {
                        start_date: getsubtractDate(1),
                        end_date: getsubtractDate(1)
                }
                return date;
        } else if (type == 'Week') {
                var date = {
                        start_date: weekStartDate(),
                        end_date: weekEndDate()
                }
                return date;
        } else if (type == 'Quarter') {
                var date = {
                        start_date: getQuarterDate('Start'),
                        end_date: getQuarterDate('End')
                }
                return date;
        } else if (type == 'Year') {
                var date = {
                        start_date: getYearDate(1, 1),
                        end_date: getYearDate(31, 12)
                }
                return date;
        } else if (type == 'All') {
                var date = {
                        start_date: '2021-01-01',
                        end_date: getYearDate(31, 12)
                }
                return date;
        }
}
common_function.getPrevCustomDate = function (date) {
        const date1 = new Date(date.start_date);
        const date2 = new Date(date.end_date);
        const diffTime = Math.abs(date2 - date1);
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

        var dateR = {
                start_date: getsubtractDateFromDate(diffDays, date.start_date),
                end_date: getsubtractDateFromDate(1, date.start_date)
        }
        return dateR;
}
common_function.changeDateFormat = function (date) {
        var date_ob = new Date(date);
        return dateFormat(date_ob, "isoDate");
}
common_function.getPrevDateFilter = function (type) {
        if (type == 'Month') {
                var date = {
                        start_date: getStartDate(1),
                        end_date: getEndDate(1)
                }
                return date;
        } else if (type == 'Today') {
                var date = {
                        start_date: getsubtractDate(1),
                        end_date: getsubtractDate(1)
                }
                return date;
        } else if (type == 'Yesterday') {
                var date = {
                        start_date: getsubtractDate(2),
                        end_date: getsubtractDate(2)
                }
                return date;
        } else if (type == 'Week') {
                var date = {
                        start_date: weekPStartDate(),
                        end_date: weekPEndDate()
                }
                return date;
        } else if (type == 'Quarter') {
                var date = {
                        start_date: getPrevQuarterDate('Start'),
                        end_date: getPrevQuarterDate('End')
                }
                return date;
        } else if (type == 'Year') {
                var date = {
                        start_date: getPreYearDate(1, 1),
                        end_date: getPreYearDate(31, 12)
                }
                return date;
        }
}

function getStartDate(type) {
        let date_ob = new Date();
        if (type == 1) {
                date_ob.setMonth(date_ob.getMonth() - 1);
        }
        var cDate = dateFormat(date_ob, "isoDate");
        var Monthdate = cDate.split('-');
        return [Monthdate[0], Monthdate[1], '01'].join('-');
}
function weekStartDate() {
        let currentDate = new Date();
        console.log(currentDate);
        // console.log(currentDate.getDate()+' = '+currentDate.getDay())
        var firstday = new Date(currentDate.setDate(currentDate.getDate() - currentDate.getDay() + 1)).toUTCString();

        //var lastday = new Date(currentDate.setDate(currentDate.getDate() - currentDate.getDay() + 7)).toUTCString();
        return dateFormat(firstday, "isoDate");
}
function weekEndDate() {
        let currentDate = new Date();
        // var firstday = new Date(currentDate.setDate(currentDate.getDate() - currentDate.getDay())).toUTCString();
        var lastday = new Date(currentDate.setDate(currentDate.getDate() - currentDate.getDay() + 7)).toUTCString();
        return dateFormat(lastday, "isoDate");
}
function getParticularDayTimestamp(lastWeekDay) {
        var currentWeekMonday = new Date().getDate() - new Date().getDay() + 1;
        return new Date().setDate(currentWeekMonday - lastWeekDay);
}
function weekPStartDate() {

        return dateFormat(getParticularDayTimestamp(8), "isoDate");
}
function weekPEndDate() {

        return dateFormat(getParticularDayTimestamp(2), "isoDate");
}
function getEndDate(type) {
        let date_ob = new Date();
        if (type == 1) {

                let dates = new Date(date_ob.getFullYear(), date_ob.getMonth(), 0)
                var cDate = dateFormat(dates, "isoDate");
                return cDate;
        }
        var cDate = dateFormat(date_ob, "isoDate");
        return cDate;
}
function getsubtractDate(days) {
        var date_ob = new Date();
        date_ob.setDate(date_ob.getDate() - days);
        var cDate = dateFormat(date_ob, "isoDate");

        return cDate;
}
function getsubtractDateFromDate(days, date) {
        var date_ob = new Date(date);
        date_ob.setDate(date_ob.getDate() - days);
        var cDate = dateFormat(date_ob, "isoDate");

        return cDate;
}
function getYearDate(days, month) {
        var date_ob = new Date();
        date_ob.setMonth(month - 1);
        date_ob.setDate(days);
        var cDate = dateFormat(date_ob, "isoDate");

        return cDate;
}
function getPreYearDate(days, month) {
        var date_ob = new Date();
        date_ob.setMonth(month - 1);
        date_ob.setDate(days);
        date_ob.setFullYear(date_ob.getFullYear() - 1);
        var cDate = dateFormat(date_ob, "isoDate");

        return cDate;
}
function getQuarterDate(val) {
        var date_ob = new Date();
        var current_Month = date_ob.getMonth() + 1;

        if (val == 'Start') {
                var month = 1;
                if (current_Month > 3 && current_Month <= 6) {
                        month = 4;
                } else if (current_Month > 6 && current_Month <= 9) {
                        month = 7;
                } else if (current_Month > 9 && current_Month <= 12) {
                        month = 10;
                }
                // console.log('month:' +month);
                date_ob.setDate(1);
                date_ob.setMonth(month - 1);

                var cDate = dateFormat(date_ob, "isoDate");
                return cDate;
        } else {
                var month = 3;
                date_ob.setMonth(month - 1);
                date_ob.setDate('31');

                if (current_Month > 3 && current_Month <= 6) {
                        month = 6;
                        date_ob.setDate('30');
                        date_ob.setMonth(month - 1);

                } else if (current_Month > 6 && current_Month <= 9) {
                        month = 9;
                        date_ob.setDate('30');
                        date_ob.setMonth(month - 1);
                } else if (current_Month > 9 && current_Month <= 12) {
                        month = 12;
                        date_ob.setDate('31');
                        date_ob.setMonth(month - 1);

                }
                // console.log('month2:' +month);


                var cDate = dateFormat(date_ob, "isoDate");
                return cDate;
        }
}
function getPrevQuarterDate(val) {
        var date_ob = new Date();
        var current_Month = date_ob.getMonth() + 1;

        if (val == 'Start') {

                if (current_Month > 3 && current_Month <= 6) {
                        month = 1;
                } else if (current_Month > 6 && current_Month <= 9) {
                        month = 4;
                } else if (current_Month > 9 && current_Month <= 12) {
                        month = 7;
                } else {
                        var month = 10;
                        date_ob.setFullYear(date_ob.getFullYear() - 1);
                }
                // if (current_Month <= 6) {
                //         month = 7;
                //         date_ob.setFullYear(date_ob.getFullYear() - 1);
                // }
                // console.log('month:' +month);
                date_ob.setDate(1);
                date_ob.setMonth(month - 1);

                var cDate = dateFormat(date_ob, "isoDate");
                return cDate;
        } else {

                console.log(current_Month);
                if (current_Month > 3 && current_Month <= 6) {
                        month = 3;

                        date_ob.setMonth(month - 1);
                        date_ob.setDate('31');

                } else if (current_Month > 6 && current_Month <= 9) {
                        month = 6;

                        date_ob.setMonth(month - 1);
                        date_ob.setDate('30');

                } else if (current_Month > 9 && current_Month <= 12) {
                        month = 9;
                        date_ob.setMonth(month - 1);
                        date_ob.setDate('30');

                } else {
                        var month = 12;
                        date_ob.setMonth(month - 1);
                        date_ob.setDate('31');

                        date_ob.setFullYear(date_ob.getFullYear() - 1);
                }

                // if (current_Month <= 6) {
                //         month = 12;
                //         date_ob.setMonth(month - 1);
                //         date_ob.setDate('31');
                //         date_ob.setFullYear(date_ob.getFullYear() - 1);
                // }
                // console.log('month2:' +month);


                var cDate = dateFormat(date_ob, "isoDate");
                return cDate;
        }
}
function getsubtracMonth(month) {
        var date_ob = new Date();
        date_ob.setMonth(date_ob.getMonth() - month);
        var cDate = dateFormat(date_ob, "isoDate");

        return cDate;
}
common_function.get_current_date = function () {
        var now = new Date();
        return dateFormat(now, "ddd");
}
common_function.getGoal_percentage = function (approved, goal) {
        var goals = (parseInt(approved) * 100) / parseInt(goal);
        var total_goals = Math.round(goals / 10) * 10;
        if(total_goals > 100){
                return 100;
        }else{
                return total_goals
        }
}
common_function.get_age = function (date) {
        var now = new Date();
        return dateFormat(now, "yyyy") - dateFormat(date, "yyyy");
}
common_function.isEmptyObject = function (obj) {
        for (var key in obj) {
                if (Object.prototype.hasOwnProperty.call(obj, key)) {
                        return false;
                }
        }
        return true;
}

common_function.getEmailTemplate = function (emailTemplate) {

        var fs = require('fs');
        const ejs = require('ejs');

        const data = ejs.render(fs.readFileSync('app/views/email_template/' + emailTemplate + '.ejs', 'utf8'),
                {
                        base_url: base_url,

                });
        return data;


}
common_function.formatPhoneNumber = function (phoneNumberString) {
        if(phoneNumberString ==''){
                return phoneNumberString;
       }else{
       var decrypted = encrypt.decrypt(phoneNumberString);
       var cleaned = ('' + decrypted).replace(/\D/g, '');
       var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
       return '(' + match[1] + ') ' + match[2] + '-' + match[3];
       }
}
common_function.formatPhoneNumber2 = function (phoneNumberString) {
        if(phoneNumberString ==''){
                return phoneNumberString;
       }else{

       var cleaned = ('' + phoneNumberString).replace(/\D/g, '');
       var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
       return '(' + match[1] + ') ' + match[2] + '-' + match[3];
       }
}
common_function.moneyFormat = function (price, sign = '$') {
        price = price ? price : 0;
        if (price !== null) {
                // console.log('cvo')
                const pieces = Math.abs(parseFloat(price)).toFixed(2).split('')
                let ii = pieces.length - 3
                while ((ii -= 3) > 0) {
                        pieces.splice(ii, 0, ',')
                }
                return sign + pieces.join('')
        } else {
                return sign + '0';
        }
}

common_function.getArrayObject = function (value, key, array) {

        if (array.length > 0) {
                return array.find(obj => obj.status_id == value);
        } else {
                return {};
        }

}

common_function.getDaysFromDate = function (date) {
        const oneDay = 24 * 60 * 60 * 1000;
        var days = Math.round(Math.abs((date - new Date()) / oneDay));
       return days;
}
common_function.getSortDayNameByDate = function (date) {
        var now = new Date(date);
        return dateFormat(now, "ddd"); // Mon = ddd and Monday = dddd ==> sort code
}

common_function.s3_upload = function (req) {
        // var AWS = require('aws-sdk');
        // var fileUpload = require('express-fileupload');
        // var s3Bucket = new AWS.S3({ params: { Bucket: 'staticwebserver', Key: 'AKIAJLVWCY24GXXWCNYA' } })
        // var data = {
        //         Key: req.body.image, // file from form
        //         Body: req.files.image.data,
        //         ACL: "public-read",
        //         ContentType: helper.getContentTypeByFile(req.body.image)
        // };
        // s3Bucket.putObject(data, function (err, data) {
        //         if (err) {
        //                 return false;
        //         } else {
        //                 return data;
        //         }
        // });


}
common_function.send_mail = function (email, subject, message) {
        var nodemailer = require('nodemailer');
        console.log('Email sent...: ');
        var transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                        user: 'info.barfling@gmail.com',
                        pass: 'B@rfling520'
                }
        });

        var mailOptions = {
                from: 'info.barfling@gmail.com',
                to: email,
                subject: subject,
                html: message
        };

        transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                        console.log("error:" + error);
                        return false;
                } else {
                        console.log('Email sent: ' + info.response);
                        return true;
                }
        });
}
common_function.get_dateExist = function (date, checkDate) {
        var D_1 = date.start_date.split("-");
        var D_2 = date.end_date.split("-");
        var D_3 = checkDate.split("-");

        var d1 = new Date(D_1[2], parseInt(D_1[1]) - 1, D_1[0]);
        var d2 = new Date(D_2[2], parseInt(D_2[1]) - 1, D_2[0]);
        var d3 = new Date(D_3[2], parseInt(D_3[1]) - 1, D_3[0]);

        if (d3 > d1 && d3 < d2) {
                return true;
        } else {
                return false;
        }
}
common_function.getSettingByKey = function (key) {

        var getvalue = getConfiguration();
        var value = '';
        getvalue.forEach(function (item) {
                if (item.key == key) {
                        // console.log(item.name)
                        value = item.value;
                }
        })
        return value;

}
function getConfiguration(){
        return [
        {key: 'GOOGLE_API_KEY', value: 'AIzaSyCRYb-tgWCilA96VP361QmNDgQqv5Y2asI'}
        ];
}

common_function.getCityByKey = function (key) {
                $('select[name="state"]').on('change',function(){
                    var state_id= $(this).val();

                    if (state_id) {
                     $.ajax({
                        url: "<%= base_url %>/settings/location/getCities/"+state_id,
                      type: "GET",
                      dataType: "json",
                      success: function(data){
                        // console.log(data);
                        // alert(data);
                        $('select[name="city"]').empty();
                        $.each(data,function(key,value){
                            $('select[name="city"]').append('<option value="'+key+'">'+value+'</option>');
                        });
                      }
                     });
                    }else {
                         $('select[name="city"]').empty();
                   }
               });

}

common_function.getDateFormat = function (key) {
        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
];
        var date = new Date(key);
        var month = monthNames[date.getMonth()];
        var dd = String(date.getDate()).padStart(2, '0');
    var year = date.getFullYear();
    var finalDate = month + ' ' + dd + ', ' + year;
    return finalDate;
}

common_function.convertTimeIntoFormat = function (time) {
        time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

        if (time.length > 1) { // If time format correct
          time = time.slice (1);  // Remove full string match value
          time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
          time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        return time.join ('');
        // console.log(time);

}

common_function.put_from_url = function (keys, imageBinary, bucket, key, callback) {

        var AWS = require('aws-sdk');


AWS.config.update(keys);
 var s3Bucket = new AWS.S3( { params: {Bucket: 'nodejssetup'} } );

        // console.log(imageBinary);
        var buf = Buffer.from(imageBinary.replace(/^data:image\/\w+;base64,/, ""),'base64')
        var data = {
          Key: key,
          Body: buf,
          ContentEncoding: 'base64',
          ContentType: 'image/jpeg'
        };
        s3Bucket.putObject(data, function(err, data){
            if (err) {
              console.log(err);
              console.log('Error uploading data: ', data);
              return false;
            } else {
              console.log('successfully uploaded the image!');
              return true;
            }
        });
}

module.exports = common_function;