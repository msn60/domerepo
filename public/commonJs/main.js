if ($(window).width() > 992) {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 40) {
            $('.topnavbar').addClass("fixed-top");
            // add padding top to show content behind navbar
            $('body').css('padding-top', $('.navbar').outerHeight() + 'px');
            $('#chatDiv2').css('visibility', 'visible');
            $('#addNewOrder2').css('visibility', 'visible');
            $('.ulfixed').css('position', 'fixed');
            $('.ulfixed').css('top', '61px');
        } else {
            $('.topnavbar').removeClass("fixed-top");
            // remove padding top from body
            $('body').css('padding-top', '0');
            $('#chatDiv2').css('visibility', 'hidden');
            $('#addNewOrder2').css('visibility', 'hidden');
            $('.ulfixed').css('position', 'absolute');
            $('.ulfixed').css('top', '114px');
        }
    });
}
$(document).ready(function () {
    $("#myInput").on('keyup', function () {
        search_table($(this).val());
    });
    function search_table(value) {
        $('#myTable tr').each(function () {
            var found = 'false';
            $(this).each(function () {
                if ($(this).text().toLowerCase().indexOf(value.toLowerCase()) >= 0)
                {
                    found = 'true';
                }
            });

            if (found == 'true')
            {
                $(this).show();
            } else {
                $(this).hide();
            }
        });
    }
});

$(document).ready(function () {
    $(".searchInput").on('keyup', function () {
        search_table($(this).val());
    });
    function search_table(value) {
        $('.searchTable tr').each(function () {
            var found = 'false';
            $(this).each(function () {
                if ($(this).text().toLowerCase().indexOf(value.toLowerCase()) >= 0)
                {
                    found = 'true';
                }
            });

            if (found == 'true')
            {
                $(this).show();
            } else {
                $(this).hide();
            }
        });
    }
});


// $(document).ready(function () {
//     $('#next1').click(function () {
//         $('#btn-add').hide();
//     });

//     $('#back2').click(function () {
//         $('#btn-add').show();
//     });
// });

// $(document).ready(function () {
//     $('#next1').click(function () {
//         $('#pageSteps').text("Step 2 of 2");
//     });

//     $('#back2').click(function () {
//         $('#pageSteps').text("Step 1 of 2");
//     });
// });

$(document).ready(function ($) {
    $(".search-dealoptions li").click(function () {
        window.location = "order_detail_pending_pickup.html";
    });
    $(".search-dealoptions li").hover(function () {
        $(this).css('cursor', 'pointer');
    });
});

//$(document).ready(function (e) {
//    var events = [];
//    var settings = {};
//    var element = document.getElementById('caleandar');
//    caleandar(element, events, settings);
//});

$(document).ready(function () {
    $('.searchNumberDiv small').on('click', function (event) {
        var email = $(this).text();
        window.location = 'mailto:' + email;
    });
});

$(".sidebar-dropdown > c").click(function () {
                $(".sidebar-submenu").slideUp(200);
                if (
                        $(this)
                        .parent()
                        .hasClass("active")
                        ) {
                    $(".sidebar-dropdown").removeClass("active");
                    $(this)
                            .parent()
                            .removeClass("active");
                } else {
                    $(".sidebar-dropdown").removeClass("active");
                    $(this)
                            .next(".sidebar-submenu")
                            .slideDown(200);
                    $(this)
                            .parent()
                            .addClass("active");
                }
            });

            $("#close-sidebar").click(function () {
                $(".page-wrapper").removeClass("toggled");
            });
            $("#show-sidebar").click(function () {
                $(".page-wrapper").addClass("toggled");
            });

            $(document).ready(function () {
                $('#addNewOrder2').mouseover(function () {
                    $('.addNewDiv').show();
                });
                $('.addNewDiv').mouseout(function () {
                    $('.addNewDiv').hide();
                });

            });

            $("#search").keyup(function () {
                if ($(this).val() == "") {
                $('#showSearchDiv').hide();
                } else {
                $('#showSearchDiv').show();
                }
                $(document).click(function () {
                $("#showSearchDiv").hide();
                });
                });

