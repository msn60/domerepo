
var base_url = base_path;
// var siteRootUrl = base_url
function confirmCommentBoxWithoutFancy(msg, handler) {
    $('body').append('<div class="dialog_box_wrap dialog4 active"><span class="dialog_overlay"></span><div class="dialog_box"><div class="dialog_box_content" data-animation="pop"><div class="dialogicon"><i class="fa fa-exclamation icon orange" aria-hidden="true"></i></div><h3>' + msg + '</h3><a href="javascript:void(0)" class="themebtn btn-default dialog_4" id="button2">No</a>  <a href="javascript:void(0)" class="themebtn btn-default dialog_4" id="button1">Yes!</a></div></div></div>');

    $(document).on("click", "#button1", function (evt) {
        $(".dialog4").remove();
        handler(true);
    });
    $(document).on("click", "#button2", function (evt) {
        $(".dialog4").remove();
        handler(false);
    });

}

var loadingMessage = 'Loading...';
$(document).ready(function () {
    $('.close').on('click', function () {
        $('#mbsmessage').hide(500);
    });
});
// function signOut(url) {
//     var auth2 = gapi.auth2.getAuthInstance();
//     auth2.signOut().then(function () {
//       console.log('User signed out.');
//     });
//     $.ajax({
//         url: url,
//         type: 'get',
//         success: function (json) {
//             if (json.error === false) {

//                 window.location.href = base_url +"login/";
//             } else {
//                 jsonErrorMessage('Something went wrong!');
//             }
//         }
//     });
//   }

function show_sidebar(){
  $(".dropdownMenu").css({'visibility':'visible'});
}
function hide_sidebar(){
  $(".dropdownMenu").css({'visibility':'hidden'});
}
var fcom = {
    makeUrl: function (controller, action, others) {

        var url;
        if (!controller)
            controller = '';
        if (!action)
            action = '';

        url = base_url + controller;

        if ('' != action)
            url += '/' + action;

        if ('' != others)
            url += '/' + others;

        return url;
    },
}
function mbsmessage(msg) {
    $('#mbsmessage').show();
    $('#mbsmessage div.msg-content').html(msg);
    setTimeout(function () {
        $('#mbsmessage').hide(500);
    }, 10000);

}
function jsonErrorMessage(msg) {
    mbsmessage(msg, true);
    $('#mbsmessage').removeClass("alert alert--info");
    $('#mbsmessage').removeClass("alert alert--success");
    $('#mbsmessage').removeClass("alert alert--process");
    $('#mbsmessage').addClass("alert alert--danger");
}

function jsonSuccessMessage(msg) {
    mbsmessage(msg, true);
    $('#mbsmessage').removeClass("alert alert--info");
    $('#mbsmessage').removeClass("alert alert--danger");
    $('#mbsmessage').removeClass("alert alert--process");
    $('#mbsmessage').addClass("alert alert--success ");
}
function jsonProcessMessage() {

    mbsmessage('Process..', true);
    $('#mbsmessage').removeClass("alert alert--danger");
    $('#mbsmessage').removeClass("alert alert--success");
    $('#mbsmessage').addClass("alert alert--process");
}
function jsonNotifyMessage() {

    mbsmessage('Loading..', true);
    $('#mbsmessage').removeClass("alert alert--danger");
    $('#mbsmessage').removeClass("alert alert--success");
    $('#mbsmessage').addClass("alert alert--process");
}
function jsonRemoveMessage() {
    // $(document).trigger('close.mbsmessage');
    $("#mbsmessage .close").click();
}
$(function () {

    $('.customfieldSelect').on('change', function () {
        jsonProcessMessage();
        $.ajax({
            url: base_url + "custom/assign_field",
            data: { table:$("#tableName").val(),id: $(this).val() },
            type: 'post',
            success: function (json) {
                if (json.error === false) {
                    jsonSuccessMessage(json.msg);
                    window.location.reload();
                } else {
                    jsonErrorMessage('Something went wrong!');
                }
            }
        });
    });
    custom_submit = function(){
        jsonProcessMessage();
        $('#submit_btn').prop('disabled', true);
        $('#form').ajaxSubmit({
            delegation: true,
            beforeSubmit: function () {
            },
            success: function (json) {
                $('#submit_btn').prop('disabled', false);
                if(json.error == false) {
                    $('.close_image').click();
                    $("#mbsmessage .close").click();
                    $("#noTable1").html(json.message);

                }else{
                    jsonErrorMessage(json.message);
                }
            },
            error: function(error){
                $('#submit_btn').prop('disabled', false);
                jsonErrorMessage('Something went wrong!');
            }
        });
        return false;
    }
    getCustomDate = function(filter_url){
        jsonProcessMessage();
        $.ajax({
            url: base_url + "orders/getCustomDate",
            data: {filter_url: filter_url},
            type: 'post',
            success: function (json) {
                if (json.error === false) {
                    $.facebox(function () {
                        updateFaceboxContent(json.message, 'md');
                    });
                } else {
                    jsonErrorMessage('Something went wrong!');
                }
            }
        });
    }
    getSearchResult = function (val) {

        $.ajax({
            url: base_url + "dashboard/getSearchResult",
            data: { deal_id: val },
            type: 'post',
            success: function (json) {
                console.log(json);
                if (json.error === false) {

                    $("#getRest").html(json.message);
                    if (val.length > 0) {
                        $(".ulfixed").css('display', 'block');
                    }

                } else {
                    jsonErrorMessage('Something went wrong!');
                }
            }
        });
    }
})