$(document).ready(function(){
    $('select[name="state"]').on('change',function(){
        var state_id= $(this).val();
        if (state_id) {
         $.ajax({
            url: base_url+"settings/location/getCities/"+state_id,
          type: "GET",
          dataType: "json",
          success: function(data){
            $('select[name="city"]').html(data.data);
          }
         });
        }else {
             $('select[name="city"]').empty();
       }
   });

  
   });
 