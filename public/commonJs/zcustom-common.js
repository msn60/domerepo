(function ($) {
	var screenHeight = $(window).height() - 100;
	window.onresize = function (event) {
		var screenHeight = $(window).height() - 100;
	};
	// $('select').select2();
	updateFaceboxContent = function (t, size) {
		console.log($(window).width());
		if (size == 'sm') {
			if ($(window).width() <= '768') {
				var wd = $(window).width() - 40;
			} else {
				var wd = '300px';
			}
			$('#facebox .contentbx').css('min-width', wd);
			$('#facebox').show().css({


				left: $(window).width() / 2 - ($('#facebox .popup').outerWidth() / 2)
			})
		} else if (size == 'md') {
			if ($(window).width() <= '768') {
				var wd = $(window).width() - 40;
			} else {
				var wd = '500px';
			}
			$('#facebox .contentbx').css('min-width', wd);
			$('#facebox').show().css({


				left: $(window).width() / 2 - ($('#facebox .popup').outerWidth() / 2)
			})
		}
		else if (size == 'lg') {
			if ($(window).width() <= '768') {
				var wd = $(window).width() - 40;
			} else {
				var wd = '900px';
			}
			$('#facebox .contentbx').css('min-width', wd);
			$('#facebox').show().css({


				left: $(window).width() / 2 - ($('#facebox .popup').outerWidth() / 2)
			})
		}else if (size == 'elg') {
			if ($(window).width() <= '768') {
				var wd = $(window).width() - 40;
			} else {
				var wd = $(window).width() - 100;
			}
			$('#facebox .contentbx').css('min-width', wd);
			$('#facebox').show().css({


				left: $(window).width() / 2 - ($('#facebox .popup').outerWidth() / 2)
			})
		}
		$("#facebox .contentbx").html(t);
		$.systemMessage.close();
		console.log('size:' + size);

		// resetFaceboxHeight();

	}
	resetFaceboxHeight = function () {
		facebocxHeight = screenHeight
		$('#facebox .contentbx').css('max-height', facebocxHeight - 50 + 'px');
		if ($('#facebox .contentbx').height() + 100 >= screenHeight) {

			$('#facebox .contentbx').css('overflow-y', 'scroll');
			$('#facebox .contentbx').css('display', 'block');
		} else {

			$('#facebox .contentbx').css('max-height', '');
			$('#facebox .contentbx').css('overflow', '');
		}

	}
	$(document).bind('reveal.facebox', function () {
		fcom.resetFaceboxHeight();
	});

	$(window).on("orientationchange", function () {
		fcom.resetFaceboxHeight();
	});

	$(document).bind('loading.facebox', function () {

		$('#facebox .contentbx').addClass('fbminwidth');
	});

	$(document).bind('afterClose.facebox', fcom.resetEditorInstance);
	$(document).bind('afterClose.facebox', function () { $('html').css('overflow', '') });

	$.systemMessage = function (data, cls) {
		initialize();
		$.systemMessage.loading();
		$.systemMessage.fillSysMessage(data, cls);
	}
	$.extend($.systemMessage, {
		settings: {
			closeimage: base_url + 'public/img/facebox/close.gif',
		},
		loading: function () {
			$('.alert').show();
		},
		fillSysMessage: function (data, cls) {
			$('.alert').removeClass('alert--success');
			$('.alert').removeClass('alert--danger');
			$('.alert').removeClass('alert--process');
			if (cls) $('.alert').addClass(cls);

			$('.alert .sysmsgcontent').html(data);
			$('.alert').fadeIn();

			if (CONF_AUTO_CLOSE_SYSTEM_MESSAGES == 1) {
				var time = CONF_TIME_AUTO_CLOSE_SYSTEM_MESSAGES * 1000;
				setTimeout(function () {
					$.systemMessage.close();
				}, time);
			}
			/* setTimeout(function() {
				$('.system_message').hide('fade', {}, 500)
			}, 5000); */
		},
		close: function () {
			$(document).trigger('close.sysmsgcontent');
		},
	});

	function initialize() {
		$('.alert .close').click($.systemMessage.close);
	}

	$(document).bind('close.sysmsgcontent', function () {
		$('.alert').fadeOut();
	});

	$.facebox.settings.loadingImage = base_url + 'public/img/facebox/loading.gif';
	$.facebox.settings.closeImage = base_url + 'public/img/facebox/closelabel.png';

	if ($.datepicker) {

		var old_goToToday = $.datepicker._gotoToday
		$.datepicker._gotoToday = function (id) {
			old_goToToday.call(this, id);
			this._selectDate(id);
			$(id).blur();
			return;
		}
	}


	refreshCaptcha = function (elem) {
		$(elem).attr('src', base_url + 'helper/captcha?sid=' + Math.random());
	}

	clearCache = function () {
		$.systemMessage(langLbl.processing, false, 'alert--process');
		fcom.ajax(fcom.makeUrl('Home', 'clear'), '', function (t) {
			window.location.reload();
		});
	}

	SelectText = function (element) {
		var doc = document
			, text = doc.getElementById(element)
			, range, selection
			;
		if (doc.body.createTextRange) {
			range = document.body.createTextRange();
			range.moveToElementText(text);
			range.select();
		} else if (window.getSelection) {
			selection = window.getSelection();
			range = document.createRange();
			range.selectNodeContents(text);
			selection.removeAllRanges();
			selection.addRange(range);
		}
	}
	getSlugUrl = function (obj, str, extra, pos) {
		if (pos == undefined)
			pos = 'pre';
		var str = str.toString().toLowerCase()
			.replace(/\s+/g, '-')           // Replace spaces with -
			.replace(/[^\w\-\/]+/g, '')       // Remove all non-word chars
			.replace(/\-\-+/g, '-')         // Replace multiple - with single -
			.replace(/^-+/, '')             // Trim - from start of text
			.replace(/-+$/, '');
		if (extra && pos == 'pre') {
			str = extra + '/' + str;
		} if (extra && pos == 'post') {
			str = str + '/' + extra;
		}

		$(obj).next().html(base_url + str);

	};


	/* $(document).click(function(event) {
		$('ul.dropdown-menu').hide();
	}); */
})(jQuery);

function getSlickSliderSettings(slidesToShow, slidesToScroll, layoutDirection) {
	slidesToShow = (typeof slidesToShow != "undefined") ? parseInt(slidesToShow) : 4;
	slidesToScroll = (typeof slidesToScroll != "undefined") ? parseInt(slidesToScroll) : 1;
	layoutDirection = (typeof layoutDirection != "undefined") ? layoutDirection : 'ltr';

	if (layoutDirection == 'rtl') {
		return {
			slidesToShow: slidesToShow,
			slidesToScroll: slidesToScroll,
			infinite: false,
			arrows: true,
			rtl: true,
			prevArrow: '<a data-role="none" class="slick-prev" aria-label="previous"></a>',
			nextArrow: '<a data-role="none" class="slick-next" aria-label="next"></a>',
			responsive: [{
				breakpoint: 1050,
				settings: {
					slidesToShow: slidesToShow - 1,
				}
			},
			{
				breakpoint: 990,
				settings: {
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 400,
				settings: {
					slidesToShow: 1,
				}
			}
			]
		}
	} else {
		return {
			slidesToShow: slidesToShow,
			slidesToScroll: slidesToScroll,
			infinite: false,
			arrows: true,
			prevArrow: '<a data-role="none" class="slick-prev" aria-label="previous"></a>',
			nextArrow: '<a data-role="none" class="slick-next" aria-label="next"></a>',
			responsive: [{
				breakpoint: 1050,
				settings: {
					slidesToShow: slidesToShow - 1,
				}
			},
			{
				breakpoint: 990,
				settings: {
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 400,
				settings: {
					slidesToShow: 1,
				}
			}
			]
		}
	}
}

(function () {

	Slugify = function (str, str_val_id, is_slugify) {
		var str = str.toString().toLowerCase()
			.replace(/\s+/g, '-')           // Replace spaces with -
			.replace(/[^\w\-]+/g, '')       // Remove all non-word chars
			.replace(/\-\-+/g, '-')         // Replace multiple - with single -
			.replace(/^-+/, '')             // Trim - from start of text
			.replace(/-+$/, '');
		if ($("#" + is_slugify).val() == 0)
			$("#" + str_val_id).val(str);
	};

	/* callChart= function(dv,$labels,$series,$position){
		
		
		new Chartist.Bar('#'+dv, {

		  labels: $labels,

		  series: [$series],



		}, {

		  stackBars: false,

		  axisY: {
			   position: $position, 
			labelInterpolationFnc: function(value) {

			  return (value / 1000) + 'k';

			}

		  }

		}).on('draw', function(data) {

		  if(data.type === 'bar') {

			data.element.attr({

			  style: 'stroke-width: 25px'

			});

		  }

		});

	} */



})();

