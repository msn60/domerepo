/*
 * facebox2 (for jQuery)
 * version: 1.3
 * @requires jQuery v1.2 or later
 * @homepage https://github.com/defunkt/facebox2
 *
 * Licensed under the MIT:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Copyright Forever Chris Wanstrath, Kyle Neath
 *
 * Usage:
 *
 *  jQuery(document).ready(function() {
 *    jQuery('a[rel*=facebox2]').facebox2()
 *  })
 *
 *  <a href="#terms" rel="facebox2">Terms</a>
 *    Loads the #terms div in the box
 *
 *  <a href="terms.html" rel="facebox2">Terms</a>
 *    Loads the terms.html page in the box
 *
 *  <a href="terms.png" rel="facebox2">Terms</a>
 *    Loads the terms.png image in the box
 *
 *
 *  You can also use it programmatically:
 *
 *    jQuery.facebox2('some html')
 *    jQuery.facebox2('some html', 'my-groovy-style')
 *
 *  The above will open a facebox2 with "some html" as the content.
 *
 *    jQuery.facebox2(function($) {
 *      $.get('blah.html', function(data) { $.facebox2(data) })
 *    })
 *
 *  The above will show a loading screen before the passed function is called,
 *  allowing for a better ajaxy experience.
 *
 *  The facebox2 function can also display an ajax page, an image, or the contents of a div:
 *
 *    jQuery.facebox2({ ajax: 'remote.html' })
 *    jQuery.facebox2({ ajax: 'remote.html' }, 'my-groovy-style')
 *    jQuery.facebox2({ image: 'stairs.jpg' })
 *    jQuery.facebox2({ image: 'stairs.jpg' }, 'my-groovy-style')
 *    jQuery.facebox2({ div: '#box' })
 *    jQuery.facebox2({ div: '#box' }, 'my-groovy-style')
 *
 *  Want to close the facebox2?  Trigger the 'close.facebox2' document event:
 *
 *    jQuery(document).trigger('close.facebox2')
 *
 *  facebox2 also has a bunch of other hooks:
 *
 *    loading.facebox2
 *    beforeReveal.facebox2
 *    reveal.facebox2 (aliased as 'afterReveal.facebox2')
 *    init.facebox2
 *    afterClose.facebox2
 *
 *  Simply bind a function to any of these hooks:
 *
 *   $(document).bind('reveal.facebox2', function() { ...stuff to do after the facebox2 and contents are revealed... })
 *
 */
(function($) {
  $.facebox2 = function(data, klass) {
    $.facebox2.loading(data.settings || [])

    if (data.ajax) fillfacebox2FromAjax(data.ajax, klass)
    else if (data.image) fillfacebox2FromImage(data.image, klass)
    else if (data.div) fillfacebox2FromHref(data.div, klass)
    else if ($.isFunction(data)) data.call($)
    else $.facebox2.reveal(data, klass)
  }

  /*
   * Public, $.facebox2 methods
   */

  $.extend($.facebox2, {
    settings: {
      opacity      : 0.95,
      overlay      : true,
      loadingImage : '/facebox2/loading.gif',
      closeImage   : '/facebox2/closelabel.png',
      imageTypes   : [ 'png', 'jpg', 'jpeg', 'gif' ],
      facebox2Html  : '\
    <div id="facebox2" style="display:none;"> \
      <div class="popup"> \
        <div class="content fbminwidth"> \
        </div> \
        <a href="#" class="close"></a> \
      </div> \
    </div>'
    },

    loading: function() {
      init()
      if ($('#facebox2 .loading').length == 1) return true
      showOverlay()

      $('#facebox2 .content').empty().
        append('<div class="loading"><img src="'+$.facebox2.settings.loadingImage+'"/></div>')

      $('#facebox2').show().css({
  
        top:	getPageScroll()[1] + (getPageHeight() / 10),
        left:	$(window).width() / 2 - ($('#facebox2 .popup').outerWidth() / 2)
      })

      $(document).bind('keydown.facebox2', function(e) {
        if (e.keyCode == 27) $.facebox2.close()
        return true
      })
      $(document).trigger('loading.facebox2')
    },

    reveal: function(data, klass) {
      $(document).trigger('beforeReveal.facebox2')
      if (klass) $('#facebox2 .content').addClass(klass)
      $('#facebox2 .content').empty().append(data)
      $('#facebox2 .popup').children().fadeIn('normal')
      $('#facebox2').css('left', $(window).width() / 2 - ($('#facebox2 .popup').outerWidth() / 2))
      $(document).trigger('reveal.facebox2').trigger('afterReveal.facebox2')
    },

    close: function() {
      $(document).trigger('close.facebox2')
      return false
    }
  })

  /*
   * Public, $.fn methods
   */

  $.fn.facebox2 = function(settings) {
    if ($(this).length == 0) return

    init(settings)

    function clickHandler() {
      $.facebox2.loading(true)

      // support for rel="facebox2.inline_popup" syntax, to add a class
      // also supports deprecated "facebox2[.inline_popup]" syntax
      var klass = this.rel.match(/facebox2\[?\.(\w+)\]?/)
      if (klass) klass = klass[1]

      fillfacebox2FromHref(this.href, klass)
      return false
    }

    return this.bind('click.facebox2', clickHandler)
  }

  /*
   * Private methods
   */

  // called one time to setup facebox2 on this page
  function init(settings) {
    if ($.facebox2.settings.inited) return true
    else $.facebox2.settings.inited = true

    $(document).trigger('init.facebox2')
    makeCompatible()

    var imageTypes = $.facebox2.settings.imageTypes.join('|')
    $.facebox2.settings.imageTypesRegexp = new RegExp('\\.(' + imageTypes + ')(\\?.*)?$', 'i')

    if (settings) $.extend($.facebox2.settings, settings)
    $('body').append($.facebox2.settings.facebox2Html)

    var preload = [ new Image(), new Image() ]
    preload[0].src = $.facebox2.settings.closeImage
    preload[1].src = $.facebox2.settings.loadingImage

    $('#facebox2').find('.b:first, .bl').each(function() {
      preload.push(new Image())
      preload.slice(-1).src = $(this).css('background-image').replace(/url\((.+)\)/, '$1')
    })

    $('#facebox2 .close')
      .click($.facebox2.close)
      .append('<img src="'
              + $.facebox2.settings.closeImage
              + '" class="close_image" title="close">')
  }

  // getPageScroll() by quirksmode.com
  function getPageScroll() {
    var xScroll, yScroll;
    if (self.pageYOffset) {
      yScroll = self.pageYOffset;
      xScroll = self.pageXOffset;
    } else if (document.documentElement && document.documentElement.scrollTop) {	 // Explorer 6 Strict
      yScroll = document.documentElement.scrollTop;
      xScroll = document.documentElement.scrollLeft;
    } else if (document.body) {// all other Explorers
      yScroll = document.body.scrollTop;
      xScroll = document.body.scrollLeft;
    }
    return new Array(xScroll,yScroll)
  }

  // Adapted from getPageSize() by quirksmode.com
  function getPageHeight() {
    var windowHeight
    if (self.innerHeight) {	// all except Explorer
      windowHeight = self.innerHeight;
    } else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
      windowHeight = document.documentElement.clientHeight;
    } else if (document.body) { // other Explorers
      windowHeight = document.body.clientHeight;
    }
    return windowHeight
  }

  // Backwards compatibility
  function makeCompatible() {
    var $s = $.facebox2.settings

    $s.loadingImage = $s.loading_image || $s.loadingImage
    $s.closeImage = $s.close_image || $s.closeImage
    $s.imageTypes = $s.image_types || $s.imageTypes
    $s.facebox2Html = $s.facebox2_html || $s.facebox2Html
  }

  // Figures out what you want to display and displays it
  // formats are:
  //     div: #id
  //   image: blah.extension
  //    ajax: anything else
  function fillfacebox2FromHref(href, klass) {
    // div
    if (href.match(/#/)) {
      var url    = window.location.href.split('#')[0]
      var target = href.replace(url,'')
      if (target == '#') return
      $.facebox2.reveal($(target).html(), klass)

    // image
    } else if (href.match($.facebox2.settings.imageTypesRegexp)) {
      fillfacebox2FromImage(href, klass)
    // ajax
    } else {
      fillfacebox2FromAjax(href, klass)
    }
  }

  function fillfacebox2FromImage(href, klass) {
   
    var image = new Image()
    image.onload = function() {
      $.facebox2.reveal('<div class="image"><img src="' + image.src + '" /></div>', klass)
    }
    image.src = href
  }

  function fillfacebox2FromAjax(href, klass) {
    $.facebox2.jqxhr = $.get(href, function(data) { $.facebox2.reveal(data, klass) })
  }

  function skipOverlay() {
    return $.facebox2.settings.overlay == false || $.facebox2.settings.opacity === null
  }

  function showOverlay() {
    if (skipOverlay()) return

    if ($('#facebox2_overlay').length == 0)
      $("body").append('<div id="facebox2_overlay" class="facebox2_hide"></div>')

    $('#facebox2_overlay').hide().addClass("facebox2_overlayBG")
      .css('opacity', $.facebox2.settings.opacity)
      .click(function() { $(document).trigger('close.facebox2') })
      .fadeIn(200)
    return false
  }

  function hideOverlay() {
    if (skipOverlay()) return

    $('#facebox2_overlay').fadeOut(200, function(){
      $("#facebox2_overlay").removeClass("facebox2_overlayBG")
      $("#facebox2_overlay").addClass("facebox2_hide")
      $("#facebox2_overlay").remove()
    })

    return false
  }

  /*
   * Bindings
   */

  $(document).bind('close.facebox2', function() {
    if ($.facebox2.jqxhr) {
      $.facebox2.jqxhr.abort()
      $.facebox2.jqxhr = null
    }
    $(document).unbind('keydown.facebox2')
    $('#facebox2').fadeOut(function() {
      $('#facebox2 .content').removeClass().addClass('content fbminwidth')
      $('#facebox2 .loading').remove()
      $(document).trigger('afterClose.facebox2')
    })
    hideOverlay()
  })

})(jQuery);
