$(function () {
    $('#form').on('submit', function () {
        $('.custom_error').html('');
        jsonProcessMessage();
        var i = 0;
if($("input[name='first_name']").val() == ""){
    $('#first_name').text('Please fill the first name');
    i++;
    // return false;
}
if($("input[name='last_name']").val() == ""){
    $('#last_name').text('Please fill the last name');
    i++;
    // return false;
}
if($("input[name='email']").val() == ""){
    $('#email').text('Please fill the email field');
    i++;
    // return false;
}
if($("input[name='salary']").val() == ""){
    $('#salary').text('Please fill the salary field');
    i++;
    // return false;
}
if($("input[name='id']").val() == ""){
    if($("input[name='password']").val() == ""){
        $('#password').text('Please fill the password field');
        i++;
    }
    if($("input[name='confirm_password']").val() == ""){
        $('#confirm_password').text('Please fill the confirm password field');
        i++;
    }
    if($("input[name='password']").val() != $("input[name='confirm_password']").val()){
        $('#confirm_password').text('Confirm password field must match with password field');
        i++;
    }
}
if(i == 0){
    
        $('#submit_btn').prop('disabled', true);
        $('#form').ajaxSubmit({
            delegation: true,
            beforeSubmit: function () {
            },
            success: function (json) {
                console.log(json);
                $('#submit_btn').prop('disabled', false);
                $("#form input,#form textarea,#form select").css({ 'border': '1px solid #d9dfe7' });
                if (json.error == false) {
                    jsonSuccessMessage(json.msg);

                    setTimeout(function () {
                        window.location.href = base_url + "employee";
                    }, 1000);

                } else if (json.error == true && json.datamsg == true) {
                    jsonRemoveMessage();
                    for (const [key, value] of Object.entries(json.data)) {
                        $("#" + key).html(value.message);
                        $("input[name='" + key + "']").css({ 'border': '1px solid red' });
                        $("select[name='" + key + "']").css({ 'border': '1px solid red' });

                    }
                } else if (json.error == true && json.datamsg == false) {
                    jsonErrorMessage(json.msg);
                } else {
                    jsonErrorMessage(json.msg);
                }
            },
            error: function (error) {
                $('#submit_btn').prop('disabled', false);
                jsonErrorMessage('Something went wrong!');
            }
        });
        return false;
    }
    else{
        return false;
    }
    });
    
    $('#leaveForm').on('submit', function () {
        $('.custom_error').html('');
        jsonProcessMessage();
        var error = 0;
            if (!$("input[name='leave_type']").is(":checked")) {
                $('#leave_type').text('Please select the leave type');
                error++;
            }
            else {
                $('#leave_type').text('');
            }
if($("input[name='leave_date']").val() == ""){
    $('#leave_date').text('Please select the leave date');
    error++;
}
if($("#emp").val() == ""){
    $('#employee_name').text('Please fill the employee name');
    error++;
}
if($("input[name='reason']").val() == ""){
    $('#reason').text('Please fill the reason');
    error++;
}
if(error == 0){
        $('#submit_btn').prop('disabled', true);
        $('#leaveForm').ajaxSubmit({
            delegation: true,
            beforeSubmit: function () {
            },
            success: function (json) {
                console.log(json);
                $('#submit_btn').prop('disabled', false);
                $("#leaveForm input,#leaveForm textarea,#leaveForm select").css({ 'border': '1px solid #d9dfe7' });
                if (json.error == false) {
                    jsonSuccessMessage(json.msg);

                    setTimeout(function () {
                        window.location.href = base_url + "employee";
                    }, 1000);

                } else if (json.error == true && json.datamsg == true) {
                    jsonRemoveMessage();
                    for (const [key, value] of Object.entries(json.data)) {
                        $("#" + key).html(value.message);
                        $("input[name='" + key + "']").css({ 'border': '1px solid red' });
                        $("select[name='" + key + "']").css({ 'border': '1px solid red' });

                    }
                } else if (json.error == true && json.datamsg == false) {
                    jsonErrorMessage(json.msg);
                } else {
                    jsonErrorMessage(json.msg);
                }
            },
            error: function (error) {
                $('#submit_btn').prop('disabled', false);
                jsonErrorMessage('Something went wrong!');
            }
        });
        return false;
    }
    else{
        return false;
    }

    });
/*This function is used to display confirmation message while delete data*/
$('.deleteMsg').on('click', function(){
var id = $(this).attr('data-id');
        confirmCommentBoxWithoutFancy('Are you sure you want to delete data?', function (outcome) {


            if (outcome) {


                $.ajax({
                    url: base_url + 'employee/delete/' + id,
                    type: 'get',
                    success: function (json) {

                        if (json.error === false) {
                            console.log(json);
                            jsonSuccessMessage(json.msg);
                            setTimeout(function () {
                                location.reload();
                            }, 1000);
                        } else {
                            jsonErrorMessage('Something went wrong!');
                        }
                    }
                });

            }

        });

    });
});

var dtToday = new Date();
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();
    var maxDate = year + '-' + month + '-' + day;
    $('#leaveDate').attr('min', maxDate);