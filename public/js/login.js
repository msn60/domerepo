
$(function () {

    
    login = function () {
        jsonProcessMessage();
        
        $('#submit_btn').prop('disabled', true);
        $('#form').ajaxSubmit({
            delegation: true,
            beforeSubmit: function () {
            },
            success: function (json) {
                console.log(json);
                $('#submit_btn').prop('disabled', false);
                $("#form small").html('');
                $("#form input,textarea").css({'border':'1px solid #d9dfe7'});
                if (json.error == false) {
                    jsonSuccessMessage(json.msg);
                    window.location.href=base_url+"employee";
                    
                } else if(json.error == true && json.datamsg== true) {
                    jsonRemoveMessage();
                    for (const [key, value] of Object.entries(json.data)) {
                        $("#"+key).html(value.message);
                        $("input[name='"+key+"']").css({'border':'1px solid red'});
                        $("textarea[name='"+key+"']").css({'border':'1px solid red'});
                      }
                }else if(json.error == true && json.datamsg== false) {
                    jsonErrorMessage(json.msg);
                }else{
                    jsonErrorMessage(json.msg);
                }
            },
            error: function(error){
                $('#submit_btn').prop('disabled', false);
                jsonErrorMessage('Something went wrong!');
            }
        });
        return false;
    }
});

